(setq inhibit-startup-screen t
      make-backup-files nil
      backup-by-copying t
      create-lockfiles nil
      auto-save-default nil
      use-dialog-box nil
      show-paren-style 'parenthesis)
(setq-default indent-tabs-mode nil
              tab-width 4)
(progn ;; UI
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (menu-bar-mode -1)
  (tooltip-mode -1)
  (blink-cursor-mode -1))
(delete-selection-mode 1)
(show-paren-mode 1)
(global-display-line-numbers-mode +1)

(setq focus-follows-mouse 1
      mouse-autoselect-window t)

(set-face-attribute 'default nil :font "PragmataPro Liga-12")

(use-package undo-tree :ensure t)
(use-package modus-themes
  :init
  (setq modus-themes-bold-constructs t
        modus-themes-italic-constructs t)
  (modus-themes-load-themes)
  :config
  (modus-themes-load-operandi)
  :ensure t)

(use-package sly
  :init
  (setq inferior-lisp-program (expand-file-name "./result/bin/sbcl"))
  (setq sly-lisp-implementations
        `((sbcl (,(expand-file-name "./result/bin/sbcl")
                 "--no-sysinit"
                 "--userinit" ,(expand-file-name "./config/sbcl-init.lisp")
                 "--load" ,(expand-file-name "./ql/setup.lisp")))))
  :ensure t)

(use-package company
  :init
  (setq company-idle-delay 0
        company-mimimum-prefix-length 1
        company-selection-wrap-around t)
  :hook prog-mode
  :ensure t)

(use-package lispy
  :hook lisp-mode           
  :ensure t)

(use-package magit
  :ensure t)

(use-package rainbow-delimiters
  :hook prog-mode
  :ensure t)

(add-hook 'prog-mode-hook 'prettify-symbols-mode)
