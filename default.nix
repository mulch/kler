{ nixpkgs ? import <nixpkgs> }:

let
  emacs-overlay = (import (builtins.fetchGit {
    url = https://github.com/nix-community/emacs-overlay;
    ref = "master";
    rev = "ffc00124f627fd5dec1d649555aa128b66ce6bc7";
  }));
  pkgs = nixpkgs {
    overlays = [ emacs-overlay ];
  };
  emacsY = pkgs.emacsWithPackagesFromUsePackage {
    config = ./config/init.el;
    defaultInitFile = true;
    package = pkgs.emacsNativeComp;
  };
in {
  env = pkgs.buildEnv {
    name = "kler-env";
    paths = with pkgs; [
      bash
      coreutils gcc curl sbcl git openssh
      openssl.out sqlite.out
      emacsY
    ];
    pathsToLink = [ "/share" "/bin" "/lib" "/include"];
    extraOutputsToInstall = [ "lib" ];
  };
}
