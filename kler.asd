(defsystem "kler"
  :depends-on ("alexandria"
               "optima"
               "quri"
               "plump"
               "cl-ppcre"
               "reblocks"
               "reblocks-ui"
               "reblocks-navigation-widget")
  :components ((:file "kler")))
