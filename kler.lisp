;; (ql:quickload '(optima quri plump cl-ppcre reblocks reblocks-ui reblocks-navigation-widget))

(in-package :cl-user)
(defpackage :kler
  (:use #:cl
        #:optima
        #:quri
        #:cl-ppcre
        #:reblocks-ui/form
        #:reblocks/html)
  (:import-from #:reblocks-navigation-widget
                #:defroutes)
  (:import-from #:reblocks/widget
                #:render
                #:update
                #:defwidget)
  (:import-from #:reblocks/actions
                #:make-js-action)
  (:import-from #:reblocks/app
                #:defapp)
  (:export #:start-server
           #:reset-server
           #:stop-server))
(in-package :kler)

(defvar *bookmark-store* (make-hash-table))
(defvar *bookmark-counter* 0)

(defwidget page ()
  ((breadcrumbs
    :initarg :breadcrumbs
    :initform nil
    :accessor page-breadcrumbs)
   (links
    :initarg :links
    :initform '(("bookmark+" . "/add/bookmark/")
                ("favorites" . "/favorites/"))
    :accessor page-links)))
(defmethod reblocks/widget:render :around ((page page))
  (with-html
    (:div :id "header"
          :class "ma3 mb2 pa2 system-sans-serif lh-solid bg-light-gray bb b--moon-gray"
          (:a :class "link black b"
              :href "/all/"
              "kler")
          (let ((breadcrumbs (page-breadcrumbs page)))
            (when breadcrumbs
              (loop for breadcrumb in breadcrumbs
                    when breadcrumb
                      do
                         (:span :class "gray"
                                :style "user-select: none;"
                                "/")
                         (:span :class "dark-gray"
                                breadcrumb))))
          (let ((links (page-links page)))
            (if links
                (:div :class "mt1"
                      (loop for link in links
                            do
                               (:a :class "dib link gray hover-dark-gray mr1"
                                   :href (cdr link)
                                   (car link)))))))
    (call-next-method)))

(defclass query ()
  ((op
     :initarg :op
     :accessor query-op)
   (args
    :initarg :args
    :accessor query-args)))
(defmacro make-query (form)
  (match form
    ((cons 'quote x) `(quote ,@x))
    ((cons sym rest) `(make-instance
                       'query
                       :op (quote ,sym)
                       :args (list ,@(mapcar (lambda (piece) (if (listp piece)
                                                            (macroexpand `(make-query ,piece))
                                                            piece))
                                             rest))))
    (f f)))
(defun compile-query (query)
  "Parse QUERY into a lambda."
  (let ((op (query-op query))
        (args (query-args query)))
    (case op
      (not
       (lambda (bookmark) (not (funcall (compile-query (first args)) bookmark))))
      (or
       (lambda (bookmark) (eval `(or ,@(mapcar (lambda (q) (funcall (compile-query q) bookmark)) args)))))
      (and
       (lambda (bookmark) (eval `(and ,@(mapcar (lambda (q) (funcall (compile-query q) bookmark)) args)))))
      (has-tag
       (lambda (bookmark) (find (first args) (bookmark-tags bookmark) :test #'string=)))
      (slot-matches
       (lambda (bookmark) (scan (create-scanner (second args) :case-insensitive-mode (third args))
                           (slot-value bookmark (first args)))))
      (otherwise
       (lambda (bookmark) (slot-value bookmark op))))))

(defwidget bookmark ()
  ((url
    :initarg :url
    :accessor bookmark-url)
   (title
    :initarg :title
    :accessor bookmark-title)
   (description
    :initarg :description
    :accessor bookmark-description
    :initform nil)
   (favorite
    :initarg :favorite
    :accessor bookmark-favorite
    :initform nil)
   (tags
    :initform nil
    :initarg :tags
    :accessor bookmark-tags)
   (uid
    :initarg :uid
    :accessor bookmark-uid)))
(defun make-bookmark (title url &key description tags)
  (let* ((uid (incf *bookmark-counter*))
         (bookmark (make-instance 'bookmark
                                  :title title
                                  :url (uri url)
                                  :description description
                                  :tags tags
                                  :favorite nil)))
    (setf (bookmark-uid bookmark) uid)
    (setf (gethash uid *bookmark-store*) bookmark)
    bookmark))
(defun get-bookmark (uid)
  (gethash uid *bookmark-store*))

(defun get-page-title (url)
  (multiple-value-bind (body status _1 _2 _3) (dex:get url)
    (declare (ignorable _1 _2 _3))
    (when (eq status 200)
      (let* ((root-node (plump:parse body))
             (title-node (car (last (plump:get-elements-by-tag-name root-node "title")))))
        (plump:text title-node)))))

(defun bookmark-update-title (bookmark)
  (let ((title (get-page-title (bookmark-url bookmark))))
    (setf (bookmark-title bookmark) title)))

(defwidget bookmark-list (page)
  ((query
    :initarg :query
    :initform nil)
   (breadcrumbs
    :initarg :breadcrumbs
    :initform '("query results"))))
(defun make-all-bookmarks-list ()
  (make-instance 'bookmark-list
                 :query nil
                 :breadcrumbs '("all")))
(defun make-filtered-bookmark-list (query &key breadcrumbs)
  (make-instance 'bookmark-list :query query :breadcrumbs breadcrumbs))
(defun make-tagged-bookmark-list ()
  (let* ((path (reblocks/request:get-path))
         (tag (second (ppcre:all-matches-as-strings "[0-9A-Za-z\-\_ ]+" path))))
    (make-instance 'bookmark-list
                   :query (make-query (has-tag tag))
                   :breadcrumbs (list "tagged" tag))))

(defun find-bookmark (query)
  "Find the first bookmark that matches QUERY."
  (loop for id in (alexandria:hash-table-keys *bookmark-store*)
        for bookmark = (get-bookmark id)
        with compiled-query = (compile-query query)
        when (funcall compiled-query bookmark)
          do (return-from find-bookmark bookmark)))
(defun find-bookmarks (query)
  "Find a list of bookmarks that match QUERY."
  (loop for id in (alexandria:hash-table-keys *bookmark-store*)
        for bookmark = (get-bookmark id)
        with compiled-query = (compile-query query)
        when (funcall compiled-query bookmark)
          collect bookmark))
(defun find-all-bookmarks ()
  (alexandria:hash-table-values *bookmark-store*))

(defun init-bookmarks-demo ()
  (clrhash *bookmark-store*)
  (make-bookmark "The Common Lisp Cookbook"
                 "https://lispcookbook.github.io/cl-cookbook/"
                 :tags '("common lisp" "books")
                 :description "The CL Cookbook aims to tackle all sorts of topics, for the beginner as for the more advanced developer.")
  (make-bookmark "Example website"
                 "https://example.com"
                 :tags '("test" "example"))
  (make-bookmark "Practical Common Lisp"
                 "https://gigamonkeys.com/book/"
                 :tags '("common lisp" "books"))
  (make-bookmark "SLY User Manual"
                 "https://joaotavora.github.io/sly/"
                 :tags '("common lisp" "libraries")
                 :description "SLY is a Common Lisp IDE for Emacs.")
  (make-bookmark "CLOG - The Common Lisp Omnificient GUI"
                 "https://github.com/rabbibotton/clog"
                 :tags '("common lisp" "libraries")))

;;; FRONTEND

(defapp kler
  :prefix "/")

(defwidget bookmark-action (page)
  ((id :initarg :id
       :initform nil
       :accessor bookmark-action-id)))

(defwidget add-bookmark-action (bookmark-action) ())
(defwidget edit-bookmark-action (bookmark-action) ())
(defwidget delete-bookmark-action (bookmark-action) ())
(defwidget fetch-title-bookmark-action (bookmark-action) ())

(defun make-bookmark-action ()
  ;; Return add, edit, delete or fetch-title depending on the path
  (let* ((path (reblocks/request:get-path))
         (type (first (all-matches-as-strings "(add|edit|delete|fetch-title)" path)))
         (id   (if (string= type "add")
                   'non-applicable
                   (parse-integer (first (all-matches-as-strings "[0-9]+" path))))))
    (cond
      ((string= type "add") (make-instance 'add-bookmark-action
                                           :links nil
                                           :breadcrumbs '("add" "bookmark")))
      ((string= type "edit") (make-instance 'edit-bookmark-action
                                            :id id
                                            :links nil
                                            :breadcrumbs '("edit" "bookmark")))
      ((string= type "delete") (make-instance 'delete-bookmark-action
                                              :id id
                                              :links nil
                                              :breadcrumbs '("delete bookmark")))
      ((string= type "fetch-title") (make-instance 'fetch-title-bookmark-action
                                                   :id id
                                                   :links nil
                                                   :breadcrumbs '("fetch-title" "bookmark"))))))

(defroutes kler-routes
    ("/" (reblocks/response:redirect "/all/"))
  ("/favorites/?"
   (make-filtered-bookmark-list (make-query (favorite))
                                :breadcrumbs '("favorites")))
  ("/tagged/[A-Za-z \-\_0-9]+/?"
   (make-tagged-bookmark-list))
  ("/(add|edit|delete|fetch-title)/bookmark(/|/[0-9]+/?)?"
   (make-bookmark-action))
  ("/all/?"
   (make-all-bookmarks-list)))

(defmethod reblocks/widget:get-css-classes ((page page))
  '("ml3" "mr3" "mb3" "system-sans-serif" "lh-solid"))

(defun get-action-form (action &key id values)
  (with-html-form (:post action)
    (:div :class "mt2"
          (:label :class "fw6 f6"
                  :for "title"
                  "title")
          (:input :type "text"
                  :name "title"
                  :value (when values (bookmark-title (get-bookmark id)))
                  :class "pa1 input-reset ba w-100"))
    (:div :class "mt2"
          (:label :class "fw6 f6"
                  :for "description"
                  "description")
          (:textarea :name "description"
                     :class "pa1 input-reset ba w-100"
                     (when values (bookmark-description (get-bookmark id))))
          (:div :class "mt2"
                (:label :class "fw6 f6"
                        :for "url"
                        "url")
                (:input :type "url"
                        :name "url"
                        :value (when values (bookmark-url (get-bookmark id)))
                        :class "pa1 input-reset ba w-100"))
          (:div :class "mt2"
                (:label :class "fw6 f6"
                        :for "tags"
                        "tags")
                (:input :type "text"
                        :name "tags"
                        :value (when values (format nil "~{~A~^,~}" (bookmark-tags (get-bookmark id))))
                        :class "pa1 input-reset ba w-100")))
    (:input :type "submit"
            :class "mt2 pl2 pr2 pt1 pb1 ba"
            :value "submit")))

(defmethod reblocks/widget:render ((widget add-bookmark-action))
  (get-action-form (lambda (&key title description url tags &allow-other-keys)
                     (make-bookmark title url
                                    :description description
                                    :tags (uiop:split-string tags :separator ","))
                     (reblocks/response:redirect "/all/"))))

(defmethod reblocks/widget:render ((widget edit-bookmark-action))
  (let ((id (bookmark-action-id widget)))
    (with-html
      (:div :class "pa2 bg-washed-blue"
            (:b "info:")
            ("editing bookmark \"~A\"" (bookmark-title (get-bookmark id))))
      (get-action-form (lambda (&key title description url tags &allow-other-keys)
                         (let ((bookmark (get-bookmark id)))
                           (setf (bookmark-title bookmark) title)
                           (setf (bookmark-description bookmark) description)
                           (setf (bookmark-url bookmark) (quri:uri url))
                           (setf (bookmark-tags bookmark) (uiop:split-string tags :separator ",")))
                         (reblocks/response:redirect "/all/"))
                       :id id
                       :values t))))

(defmethod reblocks/widget:render ((widget delete-bookmark-action))
  (let ((id (bookmark-action-id widget)))
    (with-html-form (:post (lambda (&rest _)
                             (declare (ignore _))
                             (remhash id *bookmark-store*)
                             (reblocks/response:redirect "/all/")))
      (:div :class "pa2 bg-washed-red"
            (:b "warning:")
            ("are you sure you want to delete bookmark \"~A\"" (bookmark-title (get-bookmark id))))
      (:input :type "submit"
              :class "mt2 pl2 pr2 pt1 pb1 ba"
              :value "submit"))))

(defmethod reblocks/widget:render ((widget fetch-title-bookmark-action))
  (let ((id (bookmark-action-id widget)))
    (with-html-form (:post (lambda (&rest _)
                             (declare (ignore _))
                             (bookmark-update-title (get-bookmark (bookmark-action-id widget)))
                             (reblocks/response:redirect "/all/")))
      (:div :class "pa2 bg-washed-red"
            (:b "warning:")
            ("are you sure you want to fetch the title for bookmark \"~A\"" (bookmark-title (get-bookmark id))))
      (:input :type "submit"
              :class "mt2 pl2 pr2 pt1 pb1 ba"
              :value "submit"))))

(defmethod reblocks/widget:render ((widget bookmark-list))
  (let* ((query (slot-value widget 'query))
         (bookmarks (if query (find-bookmarks query) (find-all-bookmarks))))
    (with-html
      (:div :id "bookmark-list"
            (if bookmarks
                (loop for bookmark in bookmarks
                      do (render bookmark))
                (:span :class "i gray"
                       "no bookmarks here...")
                )))))

(defmethod reblocks/widget:render ((widget bookmark))
  (with-html
    (:div :class "flex bb b--moon-gray pb2 mb2"
          (:div :class "pr2"
                :style "user-select: none;"
                (:a :class "link pointer"
                    :onclick (make-js-action (lambda (&rest args)
                                               (declare (ignorable args))
                                               (setf (bookmark-favorite widget)
                                                     (not (bookmark-favorite widget)))
                                               (update widget)))
                    (if (bookmark-favorite widget) "🌕" "🌑")))
          (:div :class "w-100"
                (:a :class "b blue hover-dark-blue link mr1"
                    :href (bookmark-url widget)
                    ("~A" (bookmark-title widget)))
                (:a :class "gray hover-mid-gray link f6"
                    :href (bookmark-url widget)
                    (format nil "(~A)" (uri-host (bookmark-url widget))))
                (:br)
                (let ((tags (bookmark-tags widget)))
                  (when (and tags (listp tags))
                    (loop for tag in tags
                          do (:a :class "dib mr1 f6 link purple hover-navy"
                                 :href (format nil "/tagged/~A/" tag)
                                 ("~A" tag)))))
                (let ((description (bookmark-description widget)))
                  (when description
                    (:p :class "dark-gray f5 ma0 pa0 mt1"
                        description)))
                (:div :class "mt1"
                      (let ((uid (bookmark-uid widget)))
                        (:a :class "mr2 f6 link gray hover-dark-gray"
                            :href (format nil "/edit/bookmark/~D/" uid)
                            "edit")
                        (:a :class "mr2 f6 link light-red hover-red"
                            :href (format nil "/delete/bookmark/~D/" uid)
                            "delete")
                        (:a :class "f6 link blue hover-dark-blue"
                            :href (format nil "/fetch-title/bookmark/~D/" uid)
                            "fetch title")))))))

(defmethod reblocks/session:init ((app kler))
  (make-kler-routes))
(defmethod reblocks/page:render :before ((app kler) inner-html &rest rest)
  (declare (ignorable rest))
  (setf (reblocks/page:get-title) "kler"))
(defmethod reblocks/dependencies:get-dependencies ((app kler))
  (list* (reblocks/dependencies:make-dependency "https://unpkg.com/tachyons@4.12.0/css/tachyons.min.css")
         (call-next-method)))

(defun start-server (&key (port 8081) debug)
  (setf reblocks-ui:*foundation-dependencies* nil)
  (setf spinneret:*suppress-inserted-spaces* t)
  (if debug
      (progn
        (reblocks/debug:on)
        (init-bookmarks-demo))
      (reblocks/debug:off))
  (reblocks/server:start :port port))
(defun stop-server ()
  (reblocks/server:stop))
(defun reset-server () (reblocks/debug:reset-latest-session))
